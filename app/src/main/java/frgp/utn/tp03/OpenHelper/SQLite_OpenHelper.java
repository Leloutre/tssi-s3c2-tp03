package frgp.utn.tp03.OpenHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import frgp.utn.tp03.Entidades.Parqueo;
import frgp.utn.tp03.Entidades.Usuario;

import androidx.annotation.Nullable;

import java.util.Objects;

public class SQLite_OpenHelper extends SQLiteOpenHelper {

    public SQLite_OpenHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = BD_Info.UsuariosCreateTable;
        String query2 = BD_Info.ParqueosCreateTable;
        db.execSQL(query);
        db.execSQL(query2);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }

    public void abrir() {
        this.getWritableDatabase();
    }

    public void cerrar() {
        this.close();
    }

    public boolean insertarUsuario(Usuario u) {
        ContentValues valores = new ContentValues();

        if(consultarUsuario(u.getMail()) != null && consultarUsuario(u.getMail()).getCount() > 0)
            return false;

        valores.put(BD_Info.UsuariosColumnaNombre, u.getNombre());
        valores.put(BD_Info.UsuariosColumnaMail, u.getMail());
        valores.put(BD_Info.UsuariosColumnaPass, u.getPass());
        this.getWritableDatabase().insert(BD_Info.UsuariosTable, null, valores);
        return true;
    }

    /*public boolean actualizarUsuario(Usuario u, String mailActual) {
        ContentValues valores = new ContentValues();

        if(consultarUsuario(mailActual) != null && consultarUsuario(mailActual).getCount() > 0)
            return false;

        Integer id = 0;
        Cursor c = consultarUsuario(mailActual);
        while(c.moveToNext())
            id = consultarUsuario(mailActual).getInt(0);

        valores.put(BD_Info.UsuariosColumnaID, id);
        valores.put(BD_Info.UsuariosColumnaNombre, u.getNombre());
        valores.put(BD_Info.UsuariosColumnaMail, u.getMail());
        valores.put(BD_Info.UsuariosColumnaPass, u.getPass());
        this.getWritableDatabase().update(BD_Info.UsuariosTable, valores, "_ID=?", new String[]{id.toString()});
        return true;
    }*/

    // En desuso
    public void eliminarUsuario(Usuario u) {
        ContentValues valores = new ContentValues();
        valores.put(BD_Info.UsuariosColumnaID, u.getId());
        String id = String.valueOf(u.getId());
        this.getWritableDatabase().delete(BD_Info.UsuariosTable, "_ID=?", new String[]{id});
    }

    public Cursor UserPass(String usuario) throws SQLException {
        Cursor cursor = null;
        String[] variables = BD_Info.SelectAllFromUsuarios;
        cursor = this.getReadableDatabase().query("usuarios", variables, "Mail = '" + usuario + "'", null, null, null, null);
        return cursor;
    }

    public Cursor consultarUsuario(String usuario) throws SQLException {
        Cursor cursor = null;
        String[] variables = BD_Info.SelectAllFromUsuarios;
        //cursor=this.getReadableDatabase()
        cursor = this.getReadableDatabase().query("usuarios", variables, "Mail = '" + usuario + "'", null, null, null, null);
        return cursor;
    }

    public void insertarParqueo(Parqueo p) {
        ContentValues valores = new ContentValues();
        valores.put(BD_Info.ParqueosColumnaMinutos, p.getMinutos());
        valores.put(BD_Info.ParqueosColumnaMatricula, p.getMatricula());
        valores.put(BD_Info.ParqueosColumnaUsuario, p.getFkUsuario().getId());
        this.getWritableDatabase().insert(BD_Info.ParqueosTable, null, valores);
    }

    public void eliminarParqueo(Integer idParqueo) {
        ContentValues valores = new ContentValues();
        valores.put(BD_Info.ParqueosColumnaID, idParqueo);
        String id = String.valueOf(idParqueo);
        this.getWritableDatabase().delete(BD_Info.ParqueosTable, "_ID=?", new String[]{id});
    }

    public Cursor parqueosXUsuario(Integer idUsuario) {
        Cursor cursor = null;
        String[] variables = BD_Info.SelectAllFromParqueos;
        cursor = this.getReadableDatabase().query("parqueos", variables, "fkUsuario = " + idUsuario, null, null, null, null);
        return cursor;
    }


}
