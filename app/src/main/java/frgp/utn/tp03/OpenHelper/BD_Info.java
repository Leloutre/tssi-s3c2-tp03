package frgp.utn.tp03.OpenHelper;

public class BD_Info {

    public static String dbName = "TP03";

    public static String UsuariosCreateTable = "CREATE TABLE IF NOT EXISTS usuarios(_ID integer primary key autoincrement, Nombre text, Mail text, Pass text);";
    public static String ParqueosCreateTable = "CREATE TABLE IF NOT EXISTS parqueos(_ID integer primary key autoincrement, fkUsuario integer, Matricula text, Minutos integer);";

    public static String UsuariosTable = "usuarios";
    public static String UsuariosColumnaID = "_ID";
    public static String UsuariosColumnaNombre = "Nombre";
    public static String UsuariosColumnaMail = "Mail";
    public static String UsuariosColumnaPass = "Pass";

    public static String ParqueosTable = "parqueos";
    public static String ParqueosColumnaID = "_ID";
    public static String ParqueosColumnaUsuario = "fkUsuario";
    public static String ParqueosColumnaMatricula = "Matricula";
    public static String ParqueosColumnaMinutos = "Minutos";

    public static String[] SelectAllFromUsuarios={UsuariosColumnaID,UsuariosColumnaNombre,UsuariosColumnaMail,UsuariosColumnaPass};
    public static String[] SelectAllFromParqueos={ParqueosColumnaID,ParqueosColumnaUsuario,ParqueosColumnaMatricula,ParqueosColumnaMinutos};


}
