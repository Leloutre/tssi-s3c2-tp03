package frgp.utn.tp03;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ParseException;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import frgp.utn.tp03.Entidades.Archivos;
import frgp.utn.tp03.Entidades.Parqueo;
import frgp.utn.tp03.Entidades.Usuario;
import frgp.utn.tp03.Entidades.Validator;
import frgp.utn.tp03.OpenHelper.BD_Info;
import frgp.utn.tp03.OpenHelper.SQLite_OpenHelper;
import frgp.utn.tp03.ui.parqueos.ParqueosFragment;
import frgp.utn.tp03.Entidades.*;

public class iniciodrawer extends AppCompatActivity {

    private SQLite_OpenHelper helper = new SQLite_OpenHelper(this, BD_Info.dbName,null,1);
    private Usuario user = new Usuario();
    private String matricula;
    private Integer tiempo;

    private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iniciodrawer);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        user = Archivos.loadSesion(this);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(),"¿Ud desea agregar un parkeo?",Toast.LENGTH_SHORT).show();
                showDialog();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerView =  navigationView.getHeaderView(0);
        TextView tvUsuario = headerView.findViewById(R.id.header_txtUsuario);
        tvUsuario.setText(user.getNombre());
        TextView tvMail = headerView.findViewById(R.id.header_txtMail);
        tvMail.setText(user.getMail());
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_parqueos, R.id.nav_micuenta)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.iniciodrawer, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public boolean cerrarSesion(MenuItem item) {
        Toast.makeText(this,"Se ha cerrado la sesion",Toast.LENGTH_SHORT).show();

        SharedPreferences pref = getApplicationContext().getSharedPreferences("Sesion", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.apply();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

        return true;
    }

    public void showDialog(){
        Log.i("ShowDialog","Context: "+this.toString());

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_parqueo);

        final EditText etMatricula = dialog.findViewById(R.id.dialog_txtMatricula);
        final EditText etTiempo = dialog.findViewById(R.id.dialog_txtTiempo);

        Button btnCancelar = dialog.findViewById(R.id.dialog_btnCancelar);
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button btnRegistrar = dialog.findViewById(R.id.dialog_btnRegistrar);
        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                matricula = etMatricula.getText().toString().trim();

                try{
                    tiempo = Integer.parseInt(etTiempo.getText().toString().trim());
                }catch (ParseException e){
                    e.getStackTrace();
                }

                if(validarParqueo(etMatricula,etTiempo,matricula,tiempo)){
                    Parqueo parqueo = new Parqueo();
                    parqueo.setMinutos(tiempo);
                    parqueo.setMatricula(matricula);
                    parqueo.setFkUsuario(user);
                    guardarParqueo(parqueo);

                    dialog.dismiss();

                    //getSupportFragmentManager().beginTransaction().detach().commit();
                    Intent intent = new Intent(getIntent());
                    finish();
                    startActivity(intent);
                }

            }
        });
        dialog.show();
    }

    public void guardarParqueo(Parqueo p){
        Log.i("guardarParqueo","Parqueo: "+p.toString());
        helper.abrir();
        helper.insertarParqueo(p);
        helper.cerrar();
    }

    public Boolean validarParqueo (TextView etMatricula, TextView etTiempo, String matricula, Integer tiempo) {

        Log.i("validarParqueo","Matricula: "+matricula+"  ---  Tiempo: "+tiempo);

        Validator validator = new Validator();

        if (!validator.validarTexto(matricula) || matricula.isEmpty()) {
            etMatricula.setError("Debe ingresar una matricula válida");
            return false;
        }
        if (!validator.validarNumeros(tiempo) || tiempo.toString().isEmpty()) {
            etTiempo.setError("Debe ingresar un número válido");
            return false;
        }
        Log.i("validarParqueo","RETURN TRUE");
        return true;
    }
}


