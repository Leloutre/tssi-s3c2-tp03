package frgp.utn.tp03.Entidades;

import android.content.Context;
import android.content.SharedPreferences;

public class Archivos {


    public static void saveSesion(Context context, Usuario usuario){

        SharedPreferences pref = context.getSharedPreferences("Sesion", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("id",usuario.getId());
        editor.putString("nombre",usuario.getNombre());
        editor.putString("correo",usuario.getMail());
        editor.putString("clave",usuario.getPass());
        editor.apply();
    }

    public static Usuario loadSesion(Context context){

        Usuario user = new Usuario();

        SharedPreferences pref = context.getSharedPreferences("Sesion", Context.MODE_PRIVATE);
        user.setId(pref.getInt("id",0));
        user.setNombre(pref.getString("nombre","No existe la sesion"));
        user.setMail(pref.getString("correo","No existe la sesion"));
        user.setPass(pref.getString("clave","No existe la sesion"));

        return user;
    }
}
