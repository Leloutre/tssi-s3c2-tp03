package frgp.utn.tp03.Entidades;

import java.io.Serializable;

public class Parqueo implements Serializable {

    private Integer idParqueo;
    private Usuario usuario;
    private String matricula;
    private Integer minutos;

    // METHODS


    //CONS
    public Parqueo() {
    }

    public Parqueo(Integer idParqueo, Usuario fkUsuario, String matricula, Integer minutos) {
        this.idParqueo = idParqueo;
        this.usuario = fkUsuario;
        this.matricula = matricula;
        this.minutos = minutos;
    }

    //GET-SET
    public Integer getIdParqueo() {
        return idParqueo;
    }

    public void setIdParqueo(Integer idParqueo) {
        this.idParqueo = idParqueo;
    }

    public Usuario getFkUsuario() {
        return usuario;
    }

    public void setFkUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public Integer getMinutos() {
        return minutos;
    }

    public void setMinutos(Integer minutos) {
        this.minutos = minutos;
    }

    @Override
    public String toString() {
        return "Parqueo{" +
                "idParqueo=" + idParqueo +
                ", usuario=" + usuario +
                ", matricula='" + matricula + '\'' +
                ", minutos=" + minutos +
                '}';
    }
}
