package frgp.utn.tp03.Entidades;

import android.database.Cursor;
import android.util.Patterns;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Validator {

    // reglas
    String pattern = "\\[_A-Za-z0-9-]";
    String emailpatter = Patterns.EMAIL_ADDRESS.pattern();
    String mailpattern =  "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";;
    String numpattern = "(\\d+)";
    String letpattern = "[\\w\\s\\d]+";

    public boolean checkPass(String pass1, String pass2){
        return pass1.equals(pass2);
    }

    public boolean validarTexto (String texto){
        return texto.matches(letpattern);
    }

    public boolean validarNumeros(Integer numero){
        Pattern p = Pattern.compile(numpattern);
        Matcher m = p.matcher(numero.toString());
        return m.matches();
    }

    public boolean validarMail(String mail){
        return mail.matches(emailpatter);
    }

    public boolean validarMailExistente(String mail, Cursor cursor){
        String data = cursor.getString(cursor.getColumnIndex("Mail"));
        return mail.equals(data);
        }

    public Validator() {
    }
}



/*
runOnUiThread(new Runnable(){
public void run() {
   Toast.makeText(getApplicationContext(), "Error",Toast.LENGTH_LONG).show();
}
});
 */