package frgp.utn.tp03.Entidades;

import java.io.Serializable;

public class Usuario implements Serializable {

    private Integer id;
    private String nombre;
    private String mail;
    private String pass;

    // CONSTRUCTORES
    public Usuario() {
    }

    public Usuario(Integer id, String nombre, String mail, String pass) {
        this.id = id;
        this.nombre = nombre;
        this.mail = mail;
        this.pass = pass;
    }

    // GET-SET

    public Integer getId() { return id; }

    public void setId(Integer id){this.id = id; }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    //TOSTRING

    @Override
    public String toString() {
        return "Usuario{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", mail='" + mail + '\'' +
                ", pass='" + pass + '\'' +
                '}';
    }
}
