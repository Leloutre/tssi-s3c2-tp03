package frgp.utn.tp03.ui.micuenta;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import frgp.utn.tp03.Entidades.Archivos;
import frgp.utn.tp03.Entidades.Usuario;
import frgp.utn.tp03.Entidades.Validator;
import frgp.utn.tp03.MainActivity;
import frgp.utn.tp03.OpenHelper.BD_Info;
import frgp.utn.tp03.OpenHelper.SQLite_OpenHelper;
import frgp.utn.tp03.R;

public class MiCuentaFragment extends Fragment {

    private SQLite_OpenHelper helper = null;
    private MiCuentaViewModel galleryViewModel;
    private Button btnCambiar;
    private TextView txtNombre, txtCorreo, txtClaveVieja;
    private String txtuser, txtcorreo, txtpass, txtpass2;
    private Usuario user = new Usuario();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        galleryViewModel =
                ViewModelProviders.of(this).get(MiCuentaViewModel.class);
        View root = inflater.inflate(R.layout.fragment_micuenta, container, false);

        txtNombre = root.findViewById(R.id.txtNombre);
        txtCorreo = root.findViewById(R.id.txtCorreo);
        /*txtClave1 = root.findViewById(R.id.usuario_etClaveNueva1);
        txtClave2 = root.findViewById(R.id.usuario_etClaveNueva2);*/
        txtClaveVieja = root.findViewById(R.id.txtClave);

        /*btnCambiar = root.findViewById(R.id.btnCambiar);*/

        user = Archivos.loadSesion(getActivity());

        txtNombre.setText(user.getNombre());
        txtCorreo.setText(user.getMail());
        txtClaveVieja.setText(user.getPass());

        /*final View act = root;*/

        /*btnCambiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtuser = txtNombre.getText().toString().trim();
                txtcorreo = txtCorreo.getText().toString().trim();
                txtpass = txtClave1.getText().toString().trim();
                txtpass2 = txtClave2.getText().toString().trim();

                if(validarCambio()){
                    Usuario usuario = new Usuario();
                    usuario.setNombre(txtuser);
                    usuario.setPass(txtpass);
                    usuario.setMail(txtcorreo);

                    helper.abrir();
                    if(!helper.actualizarUsuario(usuario, usuario.getMail())){
                        Toast.makeText(act.getContext(),"Mail ya en uso", Toast.LENGTH_SHORT).show();
                        helper.cerrar();
                    }
                    else{
                        Toast.makeText(act.getContext(),"Usuario modificado con éxito", Toast.LENGTH_SHORT).show();
                        helper.cerrar();
                    }
                }
            }
        });*/

        return root;
    }

    /*public Boolean validarCambio(){

        Validator validator = new Validator();

        if(!validator.validarTexto(txtuser) || txtuser.isEmpty()){
            txtNombre.setError("Debe ingresar un nombre de usuario válido");
            return false;
        }
        if(!validator.validarMail(txtcorreo) || txtcorreo.isEmpty()){
            txtCorreo.setError("Debe ingresar un correo válido");
            return false;
        }
        if(!txtClaveVieja.getText().toString().equals(user.getPass())){
            txtClaveVieja.setError("Contraseña actual incorrecta");
            return false;
        }
        if(!validator.validarTexto(txtpass) || txtpass.isEmpty()){
            txtClave1.setError("Debe ingresar una contraseña válida");
            return false;
        }
        if(!validator.validarTexto(txtpass2) || txtpass2.isEmpty()){
            txtClave2.setError("Debe ingresar una contraseña válida");
            return false;
        }
        if(!txtpass.equals(txtpass2)){
            txtClave2.setError("Las contraseñas deben coincidir");
            return false;
        }
        return true;
    }*/
}