package frgp.utn.tp03.ui.parqueos;

import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import frgp.utn.tp03.Entidades.Archivos;
import frgp.utn.tp03.Entidades.Parqueo;
import frgp.utn.tp03.Entidades.Usuario;
import frgp.utn.tp03.OpenHelper.BD_Info;
import frgp.utn.tp03.OpenHelper.SQLite_OpenHelper;
import frgp.utn.tp03.ParqueosAdapter;
import frgp.utn.tp03.R;

import static frgp.utn.tp03.OpenHelper.BD_Info.ParqueosColumnaID;
import static frgp.utn.tp03.OpenHelper.BD_Info.ParqueosColumnaMatricula;
import static frgp.utn.tp03.OpenHelper.BD_Info.ParqueosColumnaMinutos;

public class ParqueosFragment extends Fragment {

    private ArrayList<Parqueo> parqueos = new ArrayList<>();
    private Usuario usuario = new Usuario();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_parqueos, container, false);

        usuario = Archivos.loadSesion(root.getContext());

        SQLite_OpenHelper helper = new SQLite_OpenHelper(root.getContext(), BD_Info.dbName,null,1);
        helper.abrir();
        Cursor cursor = helper.parqueosXUsuario(usuario.getId());

        if(cursor.getCount()!=0) {
            cursor.moveToFirst();
            for(int i = 0; i < cursor.getCount(); i++){
                Parqueo p = new Parqueo();
                p.setIdParqueo(cursor.getInt(cursor.getColumnIndex(ParqueosColumnaID)));
                p.setMatricula(cursor.getString(cursor.getColumnIndex(ParqueosColumnaMatricula)));
                p.setMinutos(cursor.getInt(cursor.getColumnIndex(ParqueosColumnaMinutos)));
                parqueos.add(p);
                cursor.moveToNext();
            }
        }
        helper.cerrar();

        GridView gridview = root.findViewById(R.id.grid_parqueos);
        ParqueosAdapter parqueosAdapter = new ParqueosAdapter(this.getContext(), parqueos, getActivity());
        gridview.setAdapter(parqueosAdapter);

        return root;
    }
}