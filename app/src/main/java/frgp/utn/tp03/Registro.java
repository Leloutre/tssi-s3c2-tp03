package frgp.utn.tp03;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import frgp.utn.tp03.Entidades.Usuario;
import frgp.utn.tp03.Entidades.Validator;
import frgp.utn.tp03.OpenHelper.BD_Info;
import frgp.utn.tp03.OpenHelper.SQLite_OpenHelper;

public class Registro extends AppCompatActivity {

    private SQLite_OpenHelper helper = new SQLite_OpenHelper(this, BD_Info.dbName,null,1);
    private EditText txtNombre, txtCorreo, txtClave1, txtClave2;
    private Button btnAceptar;
    private String txtuser, txtcorreo, txtpass, txtpass2;
    private Usuario usuario = new Usuario();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        txtNombre = findViewById(R.id.txtNombre);
        txtCorreo = findViewById(R.id.txtCorreo);
        txtClave1 = findViewById(R.id.txtClave1);
        txtClave2 = findViewById(R.id.txtClave2);

        btnAceptar = findViewById(R.id.btnAceptar);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtuser = txtNombre.getText().toString().trim();
                txtcorreo = txtCorreo.getText().toString().trim();
                txtpass = txtClave1.getText().toString().trim();
                txtpass2 = txtClave2.getText().toString().trim();

                if(validarRegistro()){
                    usuario.setNombre(txtuser);
                    usuario.setPass(txtpass);
                    usuario.setMail(txtcorreo);

                    helper.abrir();
                    if(!helper.insertarUsuario(usuario)){
                        Toast.makeText(getApplicationContext(),"Usuario ya existente", Toast.LENGTH_SHORT).show();
                        helper.cerrar();
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"Usuario creado con éxito", Toast.LENGTH_SHORT).show();
                        helper.cerrar();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }
                }
            }
        });
    }

    public Boolean validarRegistro (){

        Validator validator = new Validator();

        if(!validator.validarTexto(txtuser) || txtuser.isEmpty()){
            txtNombre.setError("Debe ingresar un nombre de usuario válido");
            return false;
        }
        if(!validator.validarMail(txtcorreo) || txtcorreo.isEmpty()){
            txtCorreo.setError("Debe ingresar un correo válido");
            return false;
        }
        if(!validator.validarTexto(txtpass) || txtpass.isEmpty()){
            txtClave1.setError("Debe ingresar una contraseña válida");
            return false;
        }
        if(!validator.validarTexto(txtpass2) || txtpass2.isEmpty()){
            txtClave2.setError("Debe ingresar una contraseña válida");
            return false;
        }
        if(!txtpass.equals(txtpass2)){
            txtClave2.setError("Las contraseñas deben coincidir");
            return false;
        }
        helper.abrir();
        Cursor cursor = helper.consultarUsuario(usuario.getMail());
        if(cursor.getCount() != 0){
            if(!validator.validarMailExistente(txtcorreo,cursor)){
                txtCorreo.setError("El usuario ya existe");
                helper.cerrar();
                return false;
            }
        }
        helper.cerrar();
        return true;
    }
}
