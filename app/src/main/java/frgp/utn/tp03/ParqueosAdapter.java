package frgp.utn.tp03;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ParseException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import frgp.utn.tp03.Entidades.Parqueo;
import frgp.utn.tp03.OpenHelper.BD_Info;
import frgp.utn.tp03.OpenHelper.SQLite_OpenHelper;

public class ParqueosAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflter;
    private ArrayList<Parqueo> parqueos;
    private Activity activity;

    public ParqueosAdapter(Context context, ArrayList<Parqueo> parqueos, Activity activity) {
        this.context = context;
        this.parqueos = parqueos;
        inflter = (LayoutInflater.from(context));
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return parqueos.size();
    }

    @Override
    public Parqueo getItem(int position) {
        return parqueos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return parqueos.get(position).getIdParqueo();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.element_parqueo, null);

        TextView txtMatricula = view.findViewById(R.id.element_txtMatricula);
        TextView txtTiempo = view.findViewById(R.id.element_txtTiempo);

        txtMatricula.setText(getItem(i).getMatricula());
        txtTiempo.setText(getItem(i).getMinutos().toString());

        final int xd = getItem(i).getIdParqueo();

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(xd);
            }
        });

        return view;
    }

    public void showDialog(final int id){
        Log.i("ShowDialog","Context: "+ this.toString());

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_elimineo);

        Button btnCancelar = dialog.findViewById(R.id.dialog_btnCancelar);
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button btnEliminar = dialog.findViewById(R.id.dialog_btnEliminar);
        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLite_OpenHelper helper = new SQLite_OpenHelper(context, BD_Info.dbName,null,1);
                helper.abrir();
                helper.eliminarParqueo(id);
                dialog.dismiss();
                Intent intent = new Intent(activity.getIntent());
                activity.finish();
                activity.startActivity(intent);
                /*
                matricula = etMatricula.getText().toString().trim();

                try{
                    tiempo = Integer.parseInt(etTiempo.getText().toString().trim());
                }catch (ParseException e){
                    e.getStackTrace();
                }

                if(validarParqueo(etMatricula,etTiempo,matricula,tiempo)){
                    Parqueo parqueo = new Parqueo();
                    parqueo.setMinutos(tiempo);
                    parqueo.setMatricula(matricula);
                    parqueo.setFkUsuario(user);
                    guardarParqueo(parqueo);

                    dialog.dismiss();

                    //getSupportFragmentManager().beginTransaction().detach().commit();
                    Intent intent = new Intent(getIntent());
                    finish();
                    startActivity(intent);
                }*/
            }
        });
        dialog.show();
    }
}
