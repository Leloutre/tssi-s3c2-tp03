package frgp.utn.tp03;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;

public class ParqueosDialog extends Dialog implements android.view.View.OnClickListener {

    public Activity activity;
    public Button btnCancerlar, btnRegistrar;

    public ParqueosDialog(@NonNull Context context) {
        super(context);
        this.activity = (Activity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_parqueo);
        btnCancerlar = findViewById(R.id.dialog_btnCancelar);
        btnRegistrar = findViewById(R.id.dialog_btnRegistrar);
        btnRegistrar.setOnClickListener(this);
        btnCancerlar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.dialog_btnRegistrar:
                Toast.makeText(this.getContext(), "se ha presionado Registrar", Toast.LENGTH_SHORT).show();
                break;
            case R.id.dialog_btnCancelar:
                Toast.makeText(this.getContext(), "se ha presionado Cancelar", Toast.LENGTH_SHORT).show();
                break;
        }
        dismiss();
    }
}
