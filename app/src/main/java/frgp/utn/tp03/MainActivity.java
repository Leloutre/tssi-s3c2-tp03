package frgp.utn.tp03;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import frgp.utn.tp03.Entidades.Archivos;
import frgp.utn.tp03.Entidades.Usuario;
import frgp.utn.tp03.Entidades.Validator;
import frgp.utn.tp03.OpenHelper.SQLite_OpenHelper;
import frgp.utn.tp03.OpenHelper.BD_Info;

import static frgp.utn.tp03.OpenHelper.BD_Info.UsuariosColumnaID;
import static frgp.utn.tp03.OpenHelper.BD_Info.UsuariosColumnaMail;
import static frgp.utn.tp03.OpenHelper.BD_Info.UsuariosColumnaNombre;
import static frgp.utn.tp03.OpenHelper.BD_Info.UsuariosColumnaPass;

public class MainActivity extends AppCompatActivity {

    EditText txtUsuario, txtPass;
    String usuario, contraseña;
    TextView txtRegistro;
    Button btnAceptar;
    SQLite_OpenHelper helper = new SQLite_OpenHelper(this,BD_Info.dbName,null,1);
    Usuario user = new Usuario();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtUsuario = findViewById(R.id.txtUsuario);
        txtPass = findViewById(R.id.txtClave);
        btnAceptar = findViewById(R.id.btnLogin);
        txtRegistro = findViewById(R.id.txtRegistro);

        user = Archivos.loadSesion(this);

        if(user.getId()!=0){
            Toast.makeText(getApplicationContext(), "Bienvenido "+user.getNombre()+"", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(), iniciodrawer.class);
            intent.putExtra("usuario",user);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                usuario = txtUsuario.getText().toString().trim();
                contraseña = txtPass.getText().toString().trim();

                if(validarIngreso()){
                    Cursor cursor = helper.UserPass(usuario);

                    if(cursor.getCount()!=0){

                        cursor.moveToFirst();

                        user.setId(cursor.getInt(cursor.getColumnIndex(UsuariosColumnaID)));
                        user.setMail(cursor.getString(cursor.getColumnIndex(UsuariosColumnaMail)));
                        user.setNombre(cursor.getString(cursor.getColumnIndex(UsuariosColumnaNombre)));
                        user.setPass(cursor.getString(cursor.getColumnIndex(UsuariosColumnaPass)));

                        Log.i("Login","Usuario: "+user.toString());

                        if(user.getPass().equals(txtPass.getText().toString().trim())){
                            Toast.makeText(btnAceptar.getContext(),"Bienvenido "+user.getNombre(),Toast.LENGTH_SHORT).show();
                            Archivos.saveSesion(getApplicationContext(),user);
                            Intent intent = new Intent(getApplicationContext(), iniciodrawer.class);
                            intent.putExtra("usuario",user);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }else{
                            Toast.makeText(btnAceptar.getContext(),"Error la iniciar sesion",Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });

        txtRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Registro.class);
                startActivity(intent);
            }
        });
    }


    public Boolean validarIngreso (){

        Validator validator = new Validator();

        if(!validator.validarMail(usuario) || usuario.isEmpty()){
            txtUsuario.setError("Debe ingresar un nombre de usuario válido");
            return false;
        }
        if(!validator.validarTexto(contraseña) || usuario.isEmpty()){
            txtPass.setError("Debe ingresar una contraseña válida");
            return false;
        }
        return true;
    }
}
